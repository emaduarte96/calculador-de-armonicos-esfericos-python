from sympy import *
import math


### Funcion armonico esferico ###

def armonicoEsferico(l ,m, theta, phi):
    
    x = Symbol('x')
    t = Symbol('Theta')
    c = Symbol('phi')
    j = sqrt(-1)

    # Declaramos el factor raiz cuadrada de la funcion
    termino_raiz = sqrt(((2*l+1)/(4*math.pi))*FuncionGamma(l-m)/FuncionGamma(l+m))

    # Declaramos la formula de Rodrigues
    y = (x**2-1)**l
    p_rodrigues = (1/((2**l)*FuncionGamma(l)))*y.diff(x,l)

    # Evaluamos el polinomio de Rodrigues en coseno de theta, formando asi el polinomio de Legendr y luego lo derivamos
    p_legendre = p_rodrigues.subs(x, cos(t))
    dif_p_legendre = p_legendre.diff(t,m)

    # Declaramos la identidad de Euler
    ident_Euler = exp(m*j*c)


    p_legendre_evaluado = dif_p_legendre.subs(t, theta)
    ident_Euler_evaluado = exp(m*j*c).subs(c, phi) 

    #Estructura final del armonico esferico
    i = termino_raiz * dif_p_legendre * ident_Euler
    i_evaluado = termino_raiz * p_legendre_evaluado * ident_Euler_evaluado

    
    return i, i_evaluado


### Funcion Gamma de Euler ####
def FuncionGamma(n):

    t = Symbol('t')
    z = Symbol('z')

    f = exp(-t)*t**(z-1)

    result = integrate(f, (t,0,math.inf)).subs(z, (n+1))

    return result
